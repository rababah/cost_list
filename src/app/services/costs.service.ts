import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CostsService implements Resolve<any> {

  constructor(private _http: HttpClient) {

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._http.get("/assets/costs.json");
  }
}
