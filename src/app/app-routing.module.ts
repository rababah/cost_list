import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CostListComponent } from './cost/cost-list/cost-list.component';
import { CostsService } from './services/costs.service';
import { ExchangeRatesService } from './services/exchange-rates.service';

const routes: Routes = [{ path: "",
component: CostListComponent,
resolve: {
  costList: CostsService,
  exchangeRates: ExchangeRatesService
}}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
