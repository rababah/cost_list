import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseCurrency, CostDetails, CostItem, CostRootModel } from 'src/app/models/cost.model';
import { ExchangeRatesInterface, PaymentCurrency } from 'src/app/models/exchange-rates.model';


@Component({
  selector: 'app-cost-list',
  templateUrl: './cost-list.component.html',
  styleUrls: ['./cost-list.component.css']
})
export class CostListComponent implements OnInit {

  costList: CostRootModel | undefined;
  exchangeRates:ExchangeRatesInterface |any;
  currentCurrency :String |undefined;
  baseCurrency! :BaseCurrency  ;
  selectedQuantity: String |undefined
  exchangeCurrency! :BaseCurrency ;



  constructor(private _routes: ActivatedRoute) { }

  ngOnInit(): void {
    this._routes.data.subscribe((response: any) => {
      this.costList = response.costList;
      this.exchangeRates = response.exchangeRates;

      this.currentCurrency = this.costList?.daCurrency.currency
      this.baseCurrency = this.costList?.baseCurrency!
      this.exchangeCurrency = {"currency":this.costList?.daCurrency.currency??"" , "exchangeRate":1};


      this.costList?.baseCurrency
    })
  }

   getAmoutBasedonType(type:String, itemCosts:Array<CostDetails>) {
    //TODO Convert this types to lookups
      var selectedItem: any =  itemCosts.find((item)=>{
      if(item.type== type){
        return item.amount;
      }
      return
     })
     return selectedItem.amount;

  }

  getAmoutWithExchangeRate(type:String, itemCosts:Array<CostDetails>){
   return `${this.exchangeCurrency.currency +" "+(this.getAmoutBasedonType(type, itemCosts) * this.exchangeCurrency.exchangeRate).toFixed(2)}`
  }
  getAmoutWitBaseCurrencyRate(type:String, itemCosts:Array<CostDetails>){
    return `${this.baseCurrency.currency +" "+(this.getAmoutBasedonType(type, itemCosts) * this.baseCurrency.exchangeRate).toFixed(2)}`
   }

   getTotalWithExchangeRate(type:String,costs:Array<CostItem>){
     var totalCost =0;
     costs.forEach(element => {
      totalCost =totalCost+ (this.getAmoutBasedonType(type, element.costs) * this.exchangeCurrency.exchangeRate)
     });
     return `${this.exchangeCurrency.currency +" "+totalCost.toFixed(2)}`;
   }

   getTotalWithBaseCurrencyRate(type:String,costs:Array<CostItem>){
     var totalCost =0;
     costs.forEach(element => {
      totalCost =totalCost+ (this.getAmoutBasedonType(type, element.costs) * this.baseCurrency.exchangeRate)
     });
     return `${this.baseCurrency.currency +" "+totalCost.toFixed(2)}`;

   }

  showCommentList(costItemId:Number){
    /// TODO imporove logic here to improve performance
    this.costList?.costs.find((item=>{
      item.costItems.find(localCostItem=>{
        if(localCostItem.id == costItemId){
          localCostItem.isCommentOpens = !localCostItem.isCommentOpens
        }
      })
    }))
  }





  clickOption(e:any){
    this.selectedQuantity = e.value;
    var selectedExchangeRates =this.exchangeRates.paymentCurrencies.find((item:PaymentCurrency)=>{
      if(item.toCurrency == e.value){
        return item;
      }
      return;
    });
    this.exchangeCurrency = {"currency":e.value , "exchangeRate":selectedExchangeRates.exchangeRate};


  }





}
