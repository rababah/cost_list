import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CommentModel } from 'src/app/models/cost.model';

@Component({
  selector: 'tr [app-add-comment]',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css'],
})
export class AddCommentComponent implements OnInit {
  commentForm: FormGroup;
  types = ['Internal', 'External'];
  selectedType = 'External';
  @Output() onupdateCommentEvent = new EventEmitter<any>();

  @Input()   isUpdate:boolean = false;
  @Input() commentToUpdate :CommentModel | undefined;

  constructor() {
    this.commentForm = this.createFormGroup();
  }

  ngOnInit(): void {

  }

  ngOnChanges(){
    if(this.isUpdate) {
      this.commentForm.setValue({
        requestType: this.commentToUpdate?.type,
        text:this.commentToUpdate?.comment,

      })


    }
  }

  onSubmit() {
    var text = this.commentForm.value['text'];
    this.resetForm();
    this.commentForm.value["requestType"] ='External';

    this.onupdateCommentEvent.emit({
      isUpdate: this.isUpdate,
      Value: {
        requestType: this.selectedType,
        text:text,
        id:this.isUpdate?this.commentToUpdate?.id:""
      },
    });


  }
  createFormGroup() {
    return new FormGroup({
      requestType: new FormControl("External"),
      text: new FormControl(),
    });
  }

  clickOption(e: any) {
    this.selectedType = e.value;
  }
  resetForm(){
    this.commentForm.reset();

    this.commentForm.setValue({
      requestType: "External",
      text:"",

    })
  }

}
