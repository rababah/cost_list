import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { CommentModel } from 'src/app/models/cost.model';

@Component({
  selector: 'tr [app-comments]',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input() comments!: CommentModel[]  ; // decorate the property with @Input()

  isUpdate:boolean = false;
  commentToUpdate :CommentModel | undefined;
  constructor() { }

  ngOnInit(): void {
  }

  updateComment( $event:any){

    var mycomment :CommentModel={
      id: $event['isUpdate']?$event["Value"]["id"]:this.getRandomNumber(),
      daStage: "12",
      persona: "Current User",
      author: "Current User",
      comment: $event["Value"]["text"],
      type: $event["Value"]["requestType"],
      date:new Date((new Date()).getTime() + 24*60*60*1000),

    };
    if(!this.isUpdate){
      if(!this.comments){
        this.comments =[]
      }

      this.comments.push(mycomment);
    } else {
      this.isUpdate = false;
      this.editComment(mycomment);
    }

  }
  editComment(comment:CommentModel) {
    this.deleteComment(comment);
    this.comments.push(comment);



  }


  onClickEdit(comment:CommentModel){
    this.isUpdate = true;
    this.commentToUpdate= comment;
  }

  deleteComment(comment:CommentModel){
    this.comments = this.comments.filter((item)=>{
      return item.id !=comment.id;
    })

  }

  getRandomNumber(){
    return Math.floor((Math.random() * 500) + 1);
  }

}
