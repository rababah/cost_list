import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseCurrency } from 'src/app/models/cost.model';
import { ExchangeRatesInterface } from 'src/app/models/exchange-rates.model';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.css']
})
export class CurrenciesComponent implements OnInit {
  @Input() exchangeRates!: ExchangeRatesInterface  ; // decorate the property with @Input()
  @Input() exchangeCurrency!: BaseCurrency
  @Input() baseCurrency!: BaseCurrency
  @Output() onCurrencyChangeEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  clickOption(e:any){
    this.onCurrencyChangeEvent.emit(e)

  }

}
