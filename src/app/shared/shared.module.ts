import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsComponent } from './comments/comments.component';
import { CurrenciesComponent } from './currencies/currencies.component';
import { AddCommentComponent } from './comments/add-comment/add-comment.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CommentsComponent,
    CurrenciesComponent,
    AddCommentComponent,
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  exports:[
    CommentsComponent,
    CurrenciesComponent,
  ]

})
export class SharedModule { }
