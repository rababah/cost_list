import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DropdownListModule } from 'ngx-dropdown-list';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CostListComponent } from './cost/cost-list/cost-list.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    CostListComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    DropdownListModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
