

  export interface DaCurrency {
      currency: string;
  }

  export interface BaseCurrency {
      currency: string;
      exchangeRate: number;
  }

  export interface CostItemAlias {
      accountingCode: string;
  }

  export interface Annotation {
      id: number;
      name: string;
  }

  export interface CostDetails {
      daStage: string;
      persona: string;
      type: string;
      amount: number;
  }

  export interface CommentModel {
      id: number;
      daStage: string;
      persona: string;
      author: string;
      comment: string;
      type: string;
      date: Date;
  }

  export interface CostItem {
      id: number;
      name: string;
      isCommentOpens:Boolean;
      costItemAlias: CostItemAlias;
      annotation: Annotation;
      costs: CostDetails[];
      comments: CommentModel[];
  }

  export interface Costs {
      id: number;
      name: string;
      displayOrder: number;
      costItems: CostItem[];
  }

  export interface CostRootModel {
      daCurrency: DaCurrency;
      baseCurrency: BaseCurrency;
      costs: Costs[];
  }


