
  export interface PaymentCurrency {
      fromCurrency: string;
      toCurrency: string;
      exchangeRate: number;
  }

  export interface ExchangeRatesInterface {
      sourceCurrency: string;
      paymentCurrencies: PaymentCurrency[];
  }

